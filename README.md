# Slim Twig Flash
A Twig extension to access Slim Flash messages in templates.

## Install

Via [Composer](https://getcomposer.org/)

``` terminal
composer require glazilla/slim-twig-flash
```

Requires:

- PHP 8 or newer
- Slim Framework Flash Messages 0.4.0 or newer
- Twig 3.4.3 or newer

## Usage

- Add extension to your twig views

``` php
...
$view->addExtension(new Glazilla\Slim\Views\TwigMessages(
    new Slim\Flash\Messages()
));
...
```

- In templates use `flash()` or `flash('some_key')` to fetch messages from Flash service

``` html
...
<ul class="alert alert-danger">
    {% for msg in flash('error') %}
    <li>{{ msg }}</li>
    {% endfor %}
</ul>
...
```

## Testing

``` terminal
phpunit
```

## License
The GNU GENERAL PUBLIC LICENSE Version 3. Please see [License File](LICENSE) for more information.
