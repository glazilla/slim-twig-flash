<?php
/**
 * Slim Twig Flash.
 * 
 * @link https://bitbucket.org/glazilla/slim-twig-flash for the canonical source repository
 * @fork from kanellov/slim-twig-flash
 * @copyright Copyright (c) 2016 Vassilis Kanellopoulos <contact@kanellov.com>
 * @license GNU GPLv3 http://www.gnu.org/licenses/gpl-3.0-standalone.html
 */
namespace Glazilla\Slim\Views;

use Slim\Flash\Messages;
use Slim\Views\TwigExtension;
use Twig;

class TwigMessages extends TwigExtension
{
    /**
     * @var Messages
     */
    protected $flash;

    /**
     * Constructor.
     *
     * @param Messages $flash the Flash messages service provider
     */
    public function __construct(Messages $flash)
    {
        $this->flash = $flash;
    }

    /**
     * Extension name.
     *
     * @return string
     */
    public function getName() : string
    {
        return 'slim-twig-flash';
    }

    /**
     * Callback for twig.
     *
     * @return array
     */
    public function getFunctions() : array
    {
	return [
           new Twig\TwigFunction('flash', [$this, 'getMessages']),		    
	];
    }

    /**
     * Returns Flash messages; If key is provided then returns messages
     * for that key.
     *
     * @param string $key
     *
     * @return array
     */
    public function getMessages($key = null)
    {
        if (null !== $key) {
            return $this->flash->getMessage($key);
        }

        return $this->flash->getMessages();
    }
}
